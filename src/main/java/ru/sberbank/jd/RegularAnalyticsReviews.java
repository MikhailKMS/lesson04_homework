package ru.sberbank.jd;

public class RegularAnalyticsReviews {

    private Boolean sourceContainNewData;
    private Boolean newDataIsLoadedToOurSystem;
    private Boolean indicatorsIsCalculate;
    private Boolean reviewsIsReleased;
    private Boolean reviewsIsSended;

    public RegularAnalyticsReviews() {
        this.sourceContainNewData = null;
        this.newDataIsLoadedToOurSystem = null;
        this.indicatorsIsCalculate = null;
        this.reviewsIsReleased = null;
        this.reviewsIsSended = false;
    }

    public Boolean CheckSourceForNewData() {
        if ((int) Math.round(Math.random()) == 1) {
            sourceContainNewData = true;
            System.out.println("Источники содержат новые данные");
            return sourceContainNewData;
        } else {
            sourceContainNewData = false;
            System.out.println("Новые данные в источниках отсутсвуют, обратитесь к поставщику");
            return sourceContainNewData;
        }
    }

    public Boolean LoadNewDataFromSource() {
        if ((int) Math.round(Math.random()) == 1) {
            newDataIsLoadedToOurSystem = true;
            System.out.println("Новые данные загружены из источников");
            return newDataIsLoadedToOurSystem;
        } else {
            newDataIsLoadedToOurSystem = false;
            System.out.println("Информация не загружена, кривой формат данных");
            return newDataIsLoadedToOurSystem;
        }
    }

    public Boolean CalculateTheIndicators() {
        if ((int) Math.round(Math.random()) == 1) {
            indicatorsIsCalculate = true;
            System.out.println("Финансовые индикаторы рассчитаны");
            return indicatorsIsCalculate;
        } else {
            indicatorsIsCalculate = false;
            System.out.println("Произошел сбой в расчетах, показатели не расчитаны");
            return indicatorsIsCalculate;
        }
    }

    public Boolean ReleaseTheReviews() {
        if ((int) Math.round(Math.random()) == 1) {
            reviewsIsReleased = true;
            System.out.println("Аналитические обзоры построены");
            return reviewsIsReleased;
        } else {
            reviewsIsReleased = false;
            System.out.println("Обзоры не построены, компьютер завис");
            return reviewsIsReleased;
        }
    }

    public Boolean SendTheReviews() {
        if ((int) Math.round(Math.random()) == 1) {
            reviewsIsSended = true;
            System.out.println("Аналитические обзоры разосланы потребителям");
            return reviewsIsSended;
        } else {
            reviewsIsSended = false;
            System.out.println("Почта зависла, необходимо повторить отправку обзоров");
            return reviewsIsSended;
        }
    }

    public Boolean GetReviewsIsSended() {
        return reviewsIsSended;
    }


}
