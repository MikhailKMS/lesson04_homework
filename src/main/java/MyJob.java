import ru.sberbank.jd.RegularAnalyticsReviews;

public class MyJob {

    public static void main(String[] args) {

        int i = 1;

        RegularAnalyticsReviews regularAnalyticsReviews = new RegularAnalyticsReviews();

        while (!regularAnalyticsReviews.GetReviewsIsSended()) {

            System.out.println("Построение отчетности, попытка № " + i);

            if (i == 100) {
                System.out.println("Сегодня надоело мучаться, завтра попробуем");
                break;
            }

            if (regularAnalyticsReviews.CheckSourceForNewData()) {
                if (regularAnalyticsReviews.LoadNewDataFromSource()) {
                    if (regularAnalyticsReviews.CalculateTheIndicators()) {
                        if (regularAnalyticsReviews.ReleaseTheReviews()) {
                            if (regularAnalyticsReviews.SendTheReviews()) {
                                System.out.println("Ура, работа выполнена!!!");
                                break;
                            }

                        }
                    }
                }
            }

            i+=1;
            System.out.println("");

        }


    }


}

