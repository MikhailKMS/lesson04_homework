package ru.sberbank.jd;

import org.junit.Test;

import static org.junit.Assert.*;

public class RegularAnalyticsReviewsTest {

    @Test
    public void CheckSourceForNewDataTest() {

        RegularAnalyticsReviews regularAnalyticsReviews = new RegularAnalyticsReviews();
        Boolean result = regularAnalyticsReviews.CheckSourceForNewData();

        assertNotNull(result);

    }

    @Test
    public void LoadNewDataFromSourceTest() {

        RegularAnalyticsReviews regularAnalyticsReviews = new RegularAnalyticsReviews();
        Boolean result = regularAnalyticsReviews.LoadNewDataFromSource();

        assertNotNull(result);
    }

    @Test
    public void CalculateTheIndicatorsTest() {

        RegularAnalyticsReviews regularAnalyticsReviews = new RegularAnalyticsReviews();
        Boolean result = regularAnalyticsReviews.CalculateTheIndicators();

        assertNotNull(result);
    }

    @Test
    public void ReleaseTheReviewsTest() {

        RegularAnalyticsReviews regularAnalyticsReviews = new RegularAnalyticsReviews();
        Boolean result = regularAnalyticsReviews.ReleaseTheReviews();

        assertNotNull(result);
    }

    @Test
    public void SendTheReviewsTest() {

        RegularAnalyticsReviews regularAnalyticsReviews = new RegularAnalyticsReviews();
        Boolean result = regularAnalyticsReviews.SendTheReviews();

        assertNotNull(result);
    }

    @Test
    public void GetReviewsIsSendedTest() {

        RegularAnalyticsReviews regularAnalyticsReviews = new RegularAnalyticsReviews();
        Boolean result = regularAnalyticsReviews.SendTheReviews();

        assertNotNull(result);
    }
}